/*
Simple termostate with LCD output
Uses TMP36

*/
#include <LiquidCrystal.h>


const uint8_t lcd_RS = 12;
const uint8_t lcd_E = 11;
const uint8_t lcd_D4 = 5;
const uint8_t lcd_D5 = 4;
const uint8_t lcd_D6 = 3;
const uint8_t lcd_D7 = 2;
const int sensorPin = A0;

LiquidCrystal lcd(lcd_RS, lcd_E, lcd_D4, lcd_D5, lcd_D6, lcd_D7);


void setup() {
  lcd.begin(16, 2);
  pinMode(sensorPin, INPUT);

  lcd.clear();
  lcd.print("Temperatur: ");
  lcd.setCursor(0,1);
  
}

void display_temp(float temperatur) {

  float kelvin = temperatur + 273.15;

  lcd.setCursor(0, 1);
  lcd.print(temperatur);
  lcd.print("C, ");
  lcd.print(kelvin);
  lcd.print("K");

}

void loop() {

  static float prev_temp = 0.0;

  // Get analog reading from temp sensor
  int sensorVal = analogRead(sensorPin);

  //convert analog sensor value into a voltage
  float voltage = (sensorVal/1024.0) * 5.0;

  // convert voltage into temperatur
  float temperatur = (voltage - .5) * 100;

  if ( temperatur != prev_temp) {
    display_temp(temperatur);
  }

  prev_temp = temperatur;

  delay(1000);

}
